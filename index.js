const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use('/post', (req, res, next) => {
    if ((req.method === 'POST') && (typeof req.headers['key'] !== 'undefined')) {
        res.sendStatus(401);
    }
    else {
        next();
    }
});

app.get('/', (req, res) => {
    res.send('Hello, Express.js');
});

app.get('/hello', (req, res) => {
    res.send('Hello stranger!');
});

app.get('/hello/:userName', (req, res) => {
    res.send(`Hello, ${req.params.userName}!`);
});

app.all('/sub/*', (req, res) => {
    res.send('You requested URI: ' + req.originalUrl);
});

app.post('/post', (req, res) => {
    if(req.body.length > 0) {
        res.json(req.body);
    }
    else {
        res.sendStatus(404);
    }
});

app.listen(port, () => {
    console.log(`App listening on port ${port}!`);
});